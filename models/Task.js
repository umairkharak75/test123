const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const TaskSchema = new Schema({
 
  taskName:{
      type:String,
  },
  status:{
   type:String,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = Question = mongoose.model("Task", TaskSchema);
