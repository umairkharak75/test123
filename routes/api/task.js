const express = require('express');
const router = express.Router();
const Task = require("../../models/Task");



// @route    POST api/task
// @desc     Create task
// @access   public
router.post(
    "/",
        async (req, res) => {  
      try {
        
        const {taskName ,date,status } = req.body;
        task = new Task({
           
             taskName,
             status:status,
             date,
        });
        
        await task.save();
        res.json({ msg: 'Task Added' });
        
      }
  
       catch (err) {
        console.error(err.message);
        res.status(500).send("Server error");
      }
    }
  );


  // @route    GET api/task
// @desc     Gell all task
// @access   public
  router.get("/",  async (req, res) => {
   
    try {
      const task = await Task.find();
      
      if (!task) {
        return res.status(404).json({ msg: "no task Found" });
      }
     
      res.json({ task: task });
    } catch (err) {
      console.error(err.message);
  
      res.status(500).send("Server Error");
    }
  });
  

  router.delete("/delete/:id",  async (req, res) => {
    try {
      const task = await Task.findById(req.params.id);
      
      if (!task) {
        return res.status(404).json({ msg: "Task not Found" });
      }
      await task.remove();
      res.json({ msg: "task removed" });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  });




  router.get("/:id",  async (req, res) => {
    try {
      const task = await Task.findById(req.params.id);
      
      if (!task) {
        return res.status(404).json({ msg: "Task not Found" });
      }
    
      res.json({ task:task  });
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  });



  router.put(
    '/:id',
    async (req, res) => {
     
      const {taskName ,date,status } = req.body;
      updatedOject ={
         
           taskName,
           status,
           date,
      };
      try {
        // Using upsert option (creates new doc if no match is found):
        let task = await Task.findOneAndUpdate(
          { _id: req.params.id },
          { $set: updatedOject },
          { new: true, upsert: true, setDefaultsOnInsert: true }
        );
        res.json(task);
      } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
      }
    }
  );



 
module.exports = router;